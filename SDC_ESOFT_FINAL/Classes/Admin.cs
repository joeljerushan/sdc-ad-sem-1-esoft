﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SDC_ESOFT_FINAL.Classes;

using SDC_ESOFT_FINAL.Views.Administrator;
using SDC_ESOFT_FINAL.Views.Doctors;
using SDC_ESOFT_FINAL.Views.FrontOfficer;
using SDC_ESOFT_FINAL.Views.Manager;

//import data table
using System.Data;

//roles
//1 - manager
//2 - front officer
//3 - doctor 

namespace SDC_ESOFT_FINAL
{
    class Admin : GlobalVars
    {
        GlobalVars AuthUser = new GlobalVars();


        private string email;
        public string ADMIN_EMAIL
        {
            get { return email; }
            set { email = value; }
        }

        private string password;
        public string ADMIN_PASSWORD
        {
            get { return password; }
            set { password = value; }
        }

        private int role;
        public int ACCESS_ROLE
        {
            get { return role; }
            set { role = value; }
        }

        public bool Login()
        {
            //QUERY for find user id with role 1
            string CHECK_AUTH_USER_EXISTS = "SELECT * FROM auth_users " +
                "WHERE email='" + this.email + "' " +
                "AND password='" + this.password + "' " +
                "AND role='" + this.role + "' ";

            //assign result to data table
            DataTable USER = new DataFunctions().FillArray(CHECK_AUTH_USER_EXISTS);

            //check data table has row 
            if (Convert.ToInt32(USER.Rows.Count) == 0)
            {
                //if not show user not found message 
                MessageBox.Show("User not found", "error",MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            else
            {
                foreach (DataRow row in USER.Rows)
                {
                    string id = row["id"].ToString(); //user id
                    
                    //set authenticated user id for future tasks 
                    AuthUser.AUTH_USER = Convert.ToInt32(id); 
                }

                if (this.role == 1)
                {
                    //if role == 1 call ManagerLogin Function
                    this.ManagerLogin();
                }
                else if (this.role == 2)
                {
                    //if role == 2 call ManagerLogin Function
                    this.FrontOfficerLogin();
                }
                else if (this.role == 3)
                {
                    //if role == 3 call DoctorLogin Function
                    this.DoctorLogin();
                }
                return true;
            }
            
        }

        public bool AdminLogin()
        {

            //if incoming email & password matches predefined email and password 
            if (email == ADMIN_STATIC_USERNAME && password == ADMIN_STATIC_PASSWORD)
            {

                //set authenticated user id for future tasks 
                AuthUser.AUTH_USER = 0; //setting 0 as admin

                //move to dashboard screen here 
                AdministratorMainView AdminMainView = new AdministratorMainView();
                AdminMainView.Show();

                return true;
            }
            else
            {
                //return error message
                MessageBox.Show("User not found", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool ManagerLogin()
        {
            //move to manager's dashboard panel
            ManagerAdminPanel manager_admin_panel = new ManagerAdminPanel();
            manager_admin_panel.Show();
            return true;
   
        }

        public bool DoctorLogin()
        {
            //move to doctor's dashboard panel
            DoctorsAdminPanel doctorsAdminPanel = new DoctorsAdminPanel();
            doctorsAdminPanel.Show();
            return true;

        }

        public bool FrontOfficerLogin()
        {
            //move to front officer dashboard panel
            FrontOfficePanel FrontOfficePanel = new FrontOfficePanel();
            FrontOfficePanel.Show();

            return true;
        }

    }
}
 