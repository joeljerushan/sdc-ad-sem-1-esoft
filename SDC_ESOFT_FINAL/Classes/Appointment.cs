﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SDC_ESOFT_FINAL.Classes;
//this form class required for datagridview
using System.Windows.Forms;
//import data table
using System.Data;

namespace SDC_ESOFT_FINAL.Classes
{
    class Appointment : GlobalVars
    {
        private int id;
        public int APPOINTMENT_ID
        {
            get { return id; }
            set { id = value; }
        }

        private int patient_id;
        public int PATIENT_ID
        {
            get { return patient_id; }
            set { patient_id = value; }
        }

        private int doctor_id;
        public int DOCTOR_ID
        {
            get { return doctor_id; }
            set { doctor_id = value; }
        }

        private int created_by_id;
        public int CREATED_BY_ID
        {
            get { return created_by_id; }
            set { created_by_id = value; }
        }

        public bool Create()
        {
            //QUERY for appoitnment check by Incoming Doctor ID
            string GET_CURRENT_DOCTOR_APPOINTMENT = "SELECT * FROM doctor_appointment WHERE doctor_id = '"+ this.doctor_id + "'";

            //set return data in virtual DataTable
            DataTable CURRENT_DOCTOR_APPOINTMENT = new DataFunctions().FillArray(GET_CURRENT_DOCTOR_APPOINTMENT);

            //check if incoming doctor id currently has appointment 
            if (Convert.ToInt32(CURRENT_DOCTOR_APPOINTMENT.Rows.Count) == 0)
            {
                //if no doctor appointments create new doctor appoitment 
                string CREATE_NEW_DOCTOR_APPOINTMENT = "INSERT INTO doctor_appointment (doctor_id, count, date) " +
                    "VALUES ('"+ this.doctor_id +"', 1, '" + DateTime.Now.ToString("MM/dd/yyyy") + "')";
                
                //execute 
                new DataFunctions().ExecuteQuery(CREATE_NEW_DOCTOR_APPOINTMENT);

                //create new patient appointment query
                string CREATE_NEW_PATIENT_APPOINTMENT = "INSERT INTO patient_appointment (patient_id, doctor_id, created_by, created) " +
                    "VALUES ('" + this.patient_id + "', '" + this.doctor_id + "', '"+ AUTH_USER + "', '" + DateTime.Now.ToString("MM/dd/yyyy") + "')";
                
                //execute 
                new DataFunctions().ExecuteQuery(CREATE_NEW_PATIENT_APPOINTMENT);
                
                //show success message
                MessageBox.Show("Patient Appointment Created");

                //return
                return true;

            } else
            {
                //else check current count of incoming doctor id  
                foreach (DataRow row in CURRENT_DOCTOR_APPOINTMENT.Rows)
                {
                    string CURRENT_DATE = DateTime.Now.ToString("MM/dd/yyyy");
                    string doctor_id = row["doctor_id"].ToString(); //doctor_id
                    string count = row["count"].ToString(); //current count 
                    string date = row["date"].ToString(); //date
                    
                    //check current appointment count exceeded 8 and current date equal to doctor_appointment.count.date
                    if(Convert.ToInt32(count) >= 8 && CURRENT_DATE == date)
                    {
                        //if yes return this message
                        MessageBox.Show("This Doctor has Reached Maximum Appointments");
                    } else
                    {
                        //check current date match with appointment count date
                        if(CURRENT_DATE == date)
                        {
                            //increase count and set it to var
                            int updated_count = Convert.ToInt32(count) + 1;

                            //update query 
                            string QUERY = "UPDATE doctor_appointment " +
                                "SET count='" + updated_count + "', date='" + CURRENT_DATE + "' " +
                                "WHERE doctor_id = '" + doctor_id + "'";

                            //execute 
                            new DataFunctions().ExecuteQuery(QUERY);

                            //show success message
                            MessageBox.Show("Appointment Assigned");
                        } else
                        {
                            //if current date greater than db record date clear the count and update table 
                            //update query 
                            string QUERY = "UPDATE doctor_appointment " +
                                "SET count='1', date='" + CURRENT_DATE + "' " +
                                "WHERE doctor_id = '" + doctor_id + "'";

                            //execute 
                            new DataFunctions().ExecuteQuery(QUERY);

                            //show success message 
                            MessageBox.Show("Today's First Appointment Added");

                        }

                        //create new patient appointment query
                        string CREATE_NEW_PATIENT_APPOINTMENT = "INSERT INTO patient_appointment (patient_id, doctor_id, created_by, created) " +
                            "VALUES ('" + this.patient_id + "', '" + this.doctor_id + "', '" + AUTH_USER + "', '" + DateTime.Now.ToString("MM/dd/yyyy") + "')";

                        //execute 
                        new DataFunctions().ExecuteQuery(CREATE_NEW_PATIENT_APPOINTMENT);

                        //return
                        return true;
                    }

                }
            }
            return true;
        }

        public bool ListCompleted(DataGridView GRID_VIEW_ID)
        {
            //select completed appointments
            string QUERY = "SELECT " +
                "patient_appointment.id as 'Appointment', " +
                "patient_appointment.patient_id as 'Patient ID', " +
                "patient.name as 'Patient', " +
                "auth_users.name as 'Appoint by'," +
                "treatments.name as 'Treatment', " +
                "treatments.amount as 'Amount' " +
                "FROM patient_appointment " +
                "INNER JOIN patient ON patient_appointment.patient_id = patient.id " +
                "INNER JOIN patient_appointment_treatment ON patient.id = patient_appointment_treatment.patient_id " +
                "INNER JOIN treatments ON patient_appointment_treatment.treatment_id = treatments.id " +
                "INNER JOIN auth_users ON patient_appointment.created_by = auth_users.id " +
                "AND patient_appointment.is_complete=1";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool ListByCurrentDate(DataGridView GRID_VIEW_ID)
        {
            //selecting doctors appointments

            string QUERY = "SELECT patient_appointment.id as 'Appointment', patient_appointment.patient_id as 'Patient ID', patient.name as 'Patient', auth_users.name as 'Appoint by' " +
                "FROM patient_appointment " +
                "INNER JOIN patient ON patient_appointment.patient_id = patient.id " +
                "INNER JOIN auth_users ON patient_appointment.created_by = auth_users.id " +
                "WHERE patient_appointment.doctor_id = '" + AUTH_USER + "' " +
                "AND patient_appointment.is_complete=0 " +
                "AND patient_appointment.created = '" + DateTime.Now.ToString("MM/dd/yyyy") + "'";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool UpdateAppointment(int UPDATE_ID)
        {
            //query
            string QUERY = "UPDATE patient_appointment SET is_complete=1 WHERE id='" + UPDATE_ID + "'";

            //execute query
            new DataFunctions().ExecuteQuery(QUERY);

            //return bool
            return true;
        }

        public bool ListCompletedByDoctorId(DataGridView GRID_VIEW_ID, int DOCTOR_ID, Label LABEL)
        {

            //select completed appointments
            string QUERY = "SELECT " +
                "patient_appointment.id as 'Appointment', " +
                "patient_appointment.patient_id as 'Patient ID', patient_appointment.doctor_id as 'Doctor ID', " +
                "patient.name as 'Patient', " +
                "auth_users.name as 'Appoint by'," +
                "treatments.name as 'Treatment', " +
                "treatments.amount as 'Amount' " +
                "FROM patient_appointment " +
                "INNER JOIN patient ON patient_appointment.patient_id = patient.id " +
                "INNER JOIN patient_appointment_treatment ON patient.id = patient_appointment_treatment.patient_id " +
                "INNER JOIN treatments ON patient_appointment_treatment.treatment_id = treatments.id " +
                "INNER JOIN auth_users ON patient_appointment.created_by = auth_users.id " +
                "AND patient_appointment.is_complete=1 AND patient_appointment.doctor_id=" + DOCTOR_ID;

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            string SUM_QUERY = "SELECT SUM(treatments.amount) FROM patient_appointment INNER JOIN patient ON patient_appointment.patient_id = patient.id INNER JOIN patient_appointment_treatment ON patient.id = patient_appointment_treatment.patient_id INNER JOIN treatments ON patient_appointment_treatment.treatment_id = treatments.id INNER JOIN auth_users ON patient_appointment.created_by = auth_users.id AND patient_appointment.is_complete = 1 AND patient_appointment.doctor_id = " + DOCTOR_ID;

            //get sum of todays treatments 
            new DataFunctions().FillLabel(SUM_QUERY, LABEL);

            //return bool
            return true;
        }
    }
}
