﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDC_ESOFT_FINAL.Classes;
//this form class required for datagridview
using System.Windows.Forms;
//import data table
using System.Data;

namespace SDC_ESOFT_FINAL.Classes
{
    class AppointmentTreatment
    {

        Appointment appointment = new Appointment();

        private int id;
        public int APPOINTMENT_TREATMENT_ID
        {
            get { return id; }
            set { id = value; }
        }

        private int patient_id;
        public int PATIENT_ID
        {
            get { return patient_id; }
            set { patient_id = value; }
        }

        private int appointment_id;
        public int SELECTED_APPOINTMENT_ID
        {
            get { return appointment_id; }
            set { appointment_id = value; }
        }

        private int treatment_id;
        public int TREATMENT_ID
        {
            get { return treatment_id; }
            set { treatment_id = value; }
        }


        public bool CreateAppointmentTreatment()
        {
            //create new patient_appointment_treatment
            string QUERY = "INSERT INTO patient_appointment_treatment (patient_id, appointment_id, treatment_id, created) VALUES ('" + this.patient_id + "','" + this.appointment_id + "', '" + this.treatment_id + "', '" + DateTime.Now.ToString("MM/dd/yyyy") + "') ";

            //execute query
            new DataFunctions().ExecuteQuery(QUERY);

            //update appointment status
            appointment.UpdateAppointment(this.appointment_id);

            //return 
            return true;
        }


        public bool List(DataGridView GRID_VIEW_ID)
        {
            //query
            string QUERY = "SELECT " +
                "patient_appointment_treatment.appointment_id, patient_appointment_treatment.treatment_id, treatments.name, treatments.amount, patient_appointment_treatment.created FROM patient_appointment_treatment INNER JOIN treatments ON patient_appointment_treatment.treatment_id = treatments.id WHERE patient_appointment_treatment.patient_id='" + this.patient_id + "'";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }
    }
}
