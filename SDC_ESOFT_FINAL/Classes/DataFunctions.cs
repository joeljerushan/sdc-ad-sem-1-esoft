﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//import SqlClient which is main requirement for this class 
using System.Data.SqlClient;

//import data table
using System.Data;

//DataGridView for fill data grid 
using System.Windows.Forms;


namespace SDC_ESOFT_FINAL.Classes
{
    class DataFunctions : GlobalVars //inherit GlobalVars class 
    {

        SqlConnection CONNECTION = new SqlConnection(CONN_STRING);
        SqlCommand COMMAND;

        //sql data adapter for view data
        SqlDataAdapter DATA_ADAPTER;

        public void ExecuteQuery(string QUERY)
        {
            //first we need command 
            COMMAND = new SqlCommand(QUERY, CONNECTION);

            //open connection
            CONNECTION.Open();

            //executing query 
            COMMAND.ExecuteNonQuery();

            //closing 
            CONNECTION.Close();
        }

        public void FillGrid(string QUERY, DataGridView GRID_VIEW_ID)
        {
            //accept query and bind it to data adapter
            DATA_ADAPTER = new SqlDataAdapter(QUERY, CONNECTION);

            //create new virtual data table
            DataTable DATA_TABLE = new DataTable();

            //open connection 
            CONNECTION.Open();

            // fill data to virtual data table which created before 
            DATA_ADAPTER.Fill(DATA_TABLE);

            //close connection
            CONNECTION.Close();

            //update data which got from database to incoming Grid view 
            GRID_VIEW_ID.DataSource = DATA_TABLE;
        }

        public void FillDataSet(string QUERY, ComboBox COMBOBOX)
        {
            DATA_ADAPTER = new SqlDataAdapter(QUERY, CONNECTION);
     
            //open connection 
            CONNECTION.Open();

            DataSet DATA_SET = new DataSet();
            DATA_ADAPTER.Fill(DATA_SET, "SET");
            COMBOBOX.DisplayMember = "name";
            COMBOBOX.ValueMember = "id";
            COMBOBOX.DataSource = DATA_SET.Tables["SET"];

            //close connection
            CONNECTION.Close();

        }

        public void FillLabel(string QUERY, Label LABEL)
        {
            //first we need command 
            COMMAND = new SqlCommand(QUERY, CONNECTION);

            //open connection
            CONNECTION.Open();

            string value = null;

            //read data
            SqlDataReader reader = COMMAND.ExecuteReader();

            //run a loop to fetch single data
            while (reader.Read())
            {
                value = reader[0] as string;
                break;
            }

            //if sum is 0 return 0lkr
            if(reader[0].ToString() == "")
            {
                LABEL.Text = reader[0].ToString() + " " + "0 LKR";
            } else
            {
                LABEL.Text = reader[0].ToString() + " " + "LKR";
            }
            
            //closing 
            CONNECTION.Close();    
        }


        public DataTable FillArray(string QUERY)
        {
            //accept query and bind it to data adapter
            DATA_ADAPTER = new SqlDataAdapter(QUERY, CONNECTION);

            //create new virtual data table
            DataTable DATA_TABLE = new DataTable();

            //open connection 
            CONNECTION.Open();

            // fill data to virtual data table which created before 
            DATA_ADAPTER.Fill(DATA_TABLE);

            //close connection
            CONNECTION.Close();

            return DATA_TABLE;
        }



        public void Sort(string QUERY, DataGridView GRID_VIEW_ID)
        {

            //accept query and bind it to data adapter
            DATA_ADAPTER = new SqlDataAdapter(QUERY, CONNECTION);

            //create new virtual data table
            DataTable DATA_TABLE = new DataTable();

            //open connection 
            CONNECTION.Open();

            // fill data to virtual data table which created before 
            DATA_ADAPTER.Fill(DATA_TABLE);

            //get doctors array
            int[] doctors_array = new int[DATA_TABLE.Rows.Count];

            //loop to get all doctors experince to doctors_array 
            for (int i = 0; i < DATA_TABLE.Rows.Count; i++)
            {
                //get doctors experince value 
                doctors_array[i] = Convert.ToInt32(DATA_TABLE.Rows[i][9].ToString());
            }

            //sorting doctors_array 
            for (int i = doctors_array.Length - 1; i > 0; i--)
            {
                //with help of doctors_array.Length create another loop
                for (int j = 0; j <= i - 1; j++)
                {
                    //if doctors_array value grater than current doctors_array[items + 1] 
                    if (doctors_array[j] > doctors_array[j + 1])
                    {
                        //create most_experienced dummy variable to assign 
                        int most_experienced = doctors_array[j];

                        //assign + 1 with current doctors_array[value] 
                        doctors_array[j] = doctors_array[j + 1];

                        //set most_experienced with doctors_array[value + 1]
                        doctors_array[j + 1] = most_experienced;
                    }
                }
            }

            string msgs = string.Join(Environment.NewLine, doctors_array);
            MessageBox.Show(msgs);


            //close connection
            CONNECTION.Close();

            
            //GRID_VIEW_ID.AutoGenerateColumns = true;
            //GRID_VIEW_ID.DataSource = intArray.Select(x => new { IntValue = x }).ToList();

            //update data which got from database to incoming Grid view 
            //GRID_VIEW_ID.DataSource = DATA_TABLE;
        }

        
    }
}
