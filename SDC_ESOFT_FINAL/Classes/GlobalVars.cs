﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDC_ESOFT_FINAL.Classes
{
    class GlobalVars
    {
        //database connection string
        public static string CONN_STRING = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Joel\source\repos\SDC_ESOFT_FINAL\SDC_ESOFT_FINAL\SDC_DATABASE.mdf;Integrated Security=True";

        //Registration Fee
        public static double REGISTER_FEE = 350.00;

        //set admin username password
        public static string ADMIN_STATIC_USERNAME = "1@gmail.com";
        public static string ADMIN_STATIC_PASSWORD = "123123";

        //setting authenticated user id here
        public static int authuser;
        public int AUTH_USER
        {
            get { return authuser; }
            set { authuser = value; }
        }
    }
}
