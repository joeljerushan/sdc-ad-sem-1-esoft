﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SDC_ESOFT_FINAL.Classes;
//this form class required for datagridview
using System.Windows.Forms;

namespace SDC_ESOFT_FINAL.Classes
{
    class Patient : GlobalVars
    {
        private string id;
        public string PATIENT_ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string PATIENT_NAME
        {
            get { return name; }
            set { name = value; }
        }

        private string nic;
        public string PATIENT_NIC
        {
            get { return nic; }
            set { nic = value; }
        }

        private string address;
        public string PATIENT_ADDRESS
        {
            get { return address; }
            set { address = value; }
        }

        private string phone;
        public string PATIENT_PHONE
        {
            get { return phone; }
            set { phone = value; }
        }


        public bool Create()
        {
            //insert query 
            string QUERY = "INSERT INTO patient (name, nic, address, contact, reg_fee, created_by, created) VALUES ('"+ this.name +"', '"+ this.nic+"', '"+this.address+"', '"+this.phone+"', '"+ REGISTER_FEE + "', '"+ AUTH_USER + "', '"+ DateTime.Now.ToString("MM/dd/yyyy") + "')";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }

        public bool Search(DataGridView GRID_VIEW_ID)
        {
            //select query 
            string QUERY = "SELECT * FROM patient WHERE nic = '"+ this.nic +"'";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool List(DataGridView GRID_VIEW_ID)
        {
            //this function only accepts GRID_VIEW_ID which is comming from UI 

            //select query 
            string QUERY = "SELECT * FROM patient";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool Update()
        {
            //update query for user 
            string QUERY = "";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }

        public bool Delete()
        {
            //delete query for user 
            string QUERY = "DELETE FROM patient WHERE id='" + this.id + "'";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }

    }
}
