﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//this form class required for datagridview
using System.Windows.Forms;

namespace SDC_ESOFT_FINAL.Classes
{
    class Treatments
    {
        private string id;
        public string TREATMENT_ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string TREATMENT_NAME
        {
            get { return name; }
            set { name = value; }
        }

        private int amount;
        public int TREATMENT_AMOUNT
        {
            get { return amount; }
            set { amount = value; }
        }

        public bool Create()
        {
            //insert query 
            string QUERY = "INSERT INTO treatments (name, amount) VALUES ('" + this.name + "', '" + this.amount + "')";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }

        public bool List(DataGridView GRID_VIEW_ID)
        {
            //this function only accepts GRID_VIEW_ID which is comming from UI 

            //select query 
            string QUERY = "SELECT * FROM treatments";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool ComboBox(ComboBox comboBoxId)
        {
            ////query for filling combo box
            string QUERY = "SELECT * FROM treatments";

            ////fill combo box
            new DataFunctions().FillDataSet(QUERY, comboBoxId);

            return true;
        }

        public bool Delete()
        {
            //delete query for user 
            string QUERY = "DELETE FROM treatments WHERE id='" + this.id + "'";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }
    }
}
