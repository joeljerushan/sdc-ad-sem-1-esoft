﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//this form class required for datagridview
using System.Windows.Forms;

namespace SDC_ESOFT_FINAL.Classes
{
    class User
    {
        private string id;
        public string USER_ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string USER_NAME
        {
            get { return name; }
            set { name = value; }
        }

        private string email;
        public string USER_EMAIL
        {
            get { return email; }
            set { email = value; }
        }

        private string password;
        public string USER_PASSWORD
        {
            get { return password; }
            set { password = value; }
        }

        private int role;
        public int ACCESS_ROLE
        {
            get { return role; }
            set { role = value; }
        }

        private string nic;
        public string USER_NIC
        {
            get { return nic; }
            set { nic = value; }
        }

        private string address;
        public string USER_ADDRESS
        {
            get { return address; }
            set { address = value; }
        }

        private string phone;
        public string USER_PHONE
        {
            get { return phone; }
            set { phone = value; }
        }

        private string date_of_birth;
        public string USER_DATE_OF_BIRTH
        {
            get { return date_of_birth; }
            set { date_of_birth = value; }
        }

        private int exp_years;
        public int USER_EXP_YEARS
        {
            get { return exp_years; }
            set { exp_years = value; }
        }


        public bool CreateUser()
        {
            //insert query 
            string QUERY = "INSERT INTO auth_users (name, email, password, role, nic, address, phone, date_of_birth, exp_years, created) VALUES ('" + this.name + "', '"+this.email+"', '"+this.password+"', '"+ this.role + "', '"+ this.nic +"', '"+ this.address +"', '"+ this.phone +"', '"+ this.date_of_birth +"', '" + this.exp_years + "', '" + DateTime.Now.ToString("MM/dd/yyyy") + "')";
            new DataFunctions().ExecuteQuery(QUERY);
            return true;
        }

        public bool ListUsers(DataGridView GRID_VIEW_ID)
        {
            //this function only accepts GRID_VIEW_ID which is comming from UI 
            //select query 
            string QUERY = "SELECT * FROM auth_users WHERE role=1 OR role=2";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool ListUsersByType(DataGridView GRID_VIEW_ID, int ROLE_ID)
        {

            //select query 
            string QUERY = "SELECT * FROM auth_users WHERE role= '"+ ROLE_ID + "' ";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool SortDoctors(DataGridView GRID_VIEW_ID, int ROLE_ID)
        {

            //select query 
            string QUERY = "SELECT * FROM auth_users WHERE role= '" + ROLE_ID + "' ORDER BY exp_years DESC ";

            //string QUERY_TEST = "SELECT * FROM auth_users WHERE role= '" + ROLE_ID + "'";

            //calling the function 
            new DataFunctions().FillGrid(QUERY, GRID_VIEW_ID);
            //new DataFunctions().Sort(QUERY, GRID_VIEW_ID);

            //return bool
            return true;
        }

        public bool ComboBoxUsers(ComboBox comboBoxId, int role)
        {
            //query for filling combo box
            string QUERY = "SELECT * FROM auth_users WHERE role= '" + role + "' ";

            //fill combo box
            new DataFunctions().FillDataSet(QUERY, comboBoxId);

            return true;
        }

        public bool UpdateUser()
        {
            //update query for user 
            string QUERY = "UPDATE auth_users SET name='" + this.name + "', email='" + this.email + "', password='" + this.password + "' WHERE id='" + this.id + "'";

            //execute query 
            new DataFunctions().ExecuteQuery(QUERY);

            return true;
        }

        public bool DeleteUser()
        {
            //delete query for user 
            string QUERY = "DELETE FROM auth_users WHERE id='" + this.id + "'";

            //execute query 
            new DataFunctions().ExecuteQuery(QUERY);

            return true;
        }
    }
}
