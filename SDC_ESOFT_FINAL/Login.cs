﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDC_ESOFT_FINAL
{
    public partial class Login : Form
    {
        Admin admin = new Admin();

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            role.SelectedIndex = 1;

            //for testing
            //username.Text = "1@gmail.com";
            //password.Text = "123123";
            //role.SelectedIndex = 3;
        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            //setting role selection box value as role_index
            int role_index = role.SelectedIndex + 1;

            admin.ADMIN_EMAIL = username.Text; //get and set username
            admin.ADMIN_PASSWORD = password.Text; //get and set password
            admin.ACCESS_ROLE = role_index; //get and set role_index
            
            // if role index is 4 call administrator login function
            if (role_index == 4)
            {
                admin.AdminLogin();
            }
            else
            {
                //else normal users such as 
                //front officer, manager, doctor 
                //login function
                admin.Login();
            }
           
        }
    }
}
