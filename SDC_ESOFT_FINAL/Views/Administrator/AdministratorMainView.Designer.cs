﻿namespace SDC_ESOFT_FINAL.Views.Administrator
{
    partial class AdministratorMainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.addNewUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ds = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.userExpYears = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.userNic = new MetroFramework.Controls.MetroTextBox();
            this.updateUserButton = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.userDateOfBirth = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.userPasswordRep = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.role = new MetroFramework.Controls.MetroComboBox();
            this.userPassword = new MetroFramework.Controls.MetroTextBox();
            this.userAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.userEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.userPhone = new MetroFramework.Controls.MetroTextBox();
            this.userName = new MetroFramework.Controls.MetroTextBox();
            this.createUserButton = new MetroFramework.Controls.MetroButton();
            this.userListGrid = new System.Windows.Forms.DataGridView();
            this.userEditButton = new MetroFramework.Controls.MetroButton();
            this.userDeleteButton = new MetroFramework.Controls.MetroButton();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnSaveTreatment = new MetroFramework.Controls.MetroButton();
            this.treatmentAmount = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.treatmentName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.dataGridViewTreatments = new System.Windows.Forms.DataGridView();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.doctorsComboBox = new MetroFramework.Controls.MetroComboBox();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.gridCompletedAppointments = new System.Windows.Forms.DataGridView();
            this.totalIncomeLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroContextMenu1.SuspendLayout();
            this.ds.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userListGrid)).BeginInit();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTreatments)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompletedAppointments)).BeginInit();
            this.SuspendLayout();
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewUserToolStripMenuItem});
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(150, 26);
            // 
            // addNewUserToolStripMenuItem
            // 
            this.addNewUserToolStripMenuItem.Name = "addNewUserToolStripMenuItem";
            this.addNewUserToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.addNewUserToolStripMenuItem.Text = "Add New User";
            // 
            // ds
            // 
            this.ds.AccessibleName = "";
            this.ds.Controls.Add(this.metroTabPage1);
            this.ds.Controls.Add(this.metroTabPage2);
            this.ds.Controls.Add(this.metroTabPage3);
            this.ds.Location = new System.Drawing.Point(12, 12);
            this.ds.Name = "ds";
            this.ds.SelectedIndex = 2;
            this.ds.Size = new System.Drawing.Size(1091, 466);
            this.ds.TabIndex = 1;
            this.ds.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel10);
            this.metroTabPage1.Controls.Add(this.userExpYears);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.userNic);
            this.metroTabPage1.Controls.Add(this.updateUserButton);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.userDateOfBirth);
            this.metroTabPage1.Controls.Add(this.metroLabel6);
            this.metroTabPage1.Controls.Add(this.userPasswordRep);
            this.metroTabPage1.Controls.Add(this.metroLabel9);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.role);
            this.metroTabPage1.Controls.Add(this.userPassword);
            this.metroTabPage1.Controls.Add(this.userAddress);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel7);
            this.metroTabPage1.Controls.Add(this.userEmail);
            this.metroTabPage1.Controls.Add(this.metroLabel8);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.userPhone);
            this.metroTabPage1.Controls.Add(this.userName);
            this.metroTabPage1.Controls.Add(this.createUserButton);
            this.metroTabPage1.Controls.Add(this.userListGrid);
            this.metroTabPage1.Controls.Add(this.userEditButton);
            this.metroTabPage1.Controls.Add(this.userDeleteButton);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1083, 424);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "User Management";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            this.metroTabPage1.Click += new System.EventHandler(this.metroTabPage1_Click);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(659, 311);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(101, 19);
            this.metroLabel10.TabIndex = 35;
            this.metroLabel10.Text = "Experience Year";
            // 
            // userExpYears
            // 
            this.userExpYears.DisplayFocus = true;
            this.userExpYears.FormattingEnabled = true;
            this.userExpYears.ItemHeight = 23;
            this.userExpYears.Items.AddRange(new object[] {
            "1 Year",
            "2 Years",
            "5 Years and Up",
            "10 Years and Up",
            "20 Years and Up",
            "25 Years and Up",
            "None"});
            this.userExpYears.Location = new System.Drawing.Point(658, 333);
            this.userExpYears.Name = "userExpYears";
            this.userExpYears.Size = new System.Drawing.Size(192, 29);
            this.userExpYears.TabIndex = 34;
            this.userExpYears.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(876, 249);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(84, 19);
            this.metroLabel1.TabIndex = 26;
            this.metroLabel1.Text = "NIC Number";
            // 
            // userNic
            // 
            // 
            // 
            // 
            this.userNic.CustomButton.Image = null;
            this.userNic.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userNic.CustomButton.Name = "";
            this.userNic.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userNic.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userNic.CustomButton.TabIndex = 1;
            this.userNic.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userNic.CustomButton.UseSelectable = true;
            this.userNic.CustomButton.Visible = false;
            this.userNic.Lines = new string[0];
            this.userNic.Location = new System.Drawing.Point(877, 271);
            this.userNic.MaxLength = 32767;
            this.userNic.Name = "userNic";
            this.userNic.PasswordChar = '\0';
            this.userNic.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userNic.SelectedText = "";
            this.userNic.SelectionLength = 0;
            this.userNic.SelectionStart = 0;
            this.userNic.ShortcutsEnabled = true;
            this.userNic.Size = new System.Drawing.Size(191, 23);
            this.userNic.TabIndex = 25;
            this.userNic.UseSelectable = true;
            this.userNic.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userNic.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // updateUserButton
            // 
            this.updateUserButton.Location = new System.Drawing.Point(442, 379);
            this.updateUserButton.Name = "updateUserButton";
            this.updateUserButton.Size = new System.Drawing.Size(191, 23);
            this.updateUserButton.TabIndex = 24;
            this.updateUserButton.Text = "Update User";
            this.updateUserButton.UseSelectable = true;
            this.updateUserButton.Click += new System.EventHandler(this.updateUserButton_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(658, 249);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(115, 19);
            this.metroLabel5.TabIndex = 20;
            this.metroLabel5.Text = "Confirm Password";
            // 
            // userDateOfBirth
            // 
            this.userDateOfBirth.Location = new System.Drawing.Point(442, 333);
            this.userDateOfBirth.MinimumSize = new System.Drawing.Size(0, 29);
            this.userDateOfBirth.Name = "userDateOfBirth";
            this.userDateOfBirth.Size = new System.Drawing.Size(190, 29);
            this.userDateOfBirth.TabIndex = 33;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(879, 311);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(65, 19);
            this.metroLabel6.TabIndex = 23;
            this.metroLabel6.Text = "User Role";
            // 
            // userPasswordRep
            // 
            // 
            // 
            // 
            this.userPasswordRep.CustomButton.Image = null;
            this.userPasswordRep.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPasswordRep.CustomButton.Name = "";
            this.userPasswordRep.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPasswordRep.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPasswordRep.CustomButton.TabIndex = 1;
            this.userPasswordRep.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPasswordRep.CustomButton.UseSelectable = true;
            this.userPasswordRep.CustomButton.Visible = false;
            this.userPasswordRep.Lines = new string[0];
            this.userPasswordRep.Location = new System.Drawing.Point(659, 271);
            this.userPasswordRep.MaxLength = 32767;
            this.userPasswordRep.Name = "userPasswordRep";
            this.userPasswordRep.PasswordChar = '\0';
            this.userPasswordRep.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPasswordRep.SelectedText = "";
            this.userPasswordRep.SelectionLength = 0;
            this.userPasswordRep.SelectionStart = 0;
            this.userPasswordRep.ShortcutsEnabled = true;
            this.userPasswordRep.Size = new System.Drawing.Size(191, 23);
            this.userPasswordRep.TabIndex = 19;
            this.userPasswordRep.UseSelectable = true;
            this.userPasswordRep.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPasswordRep.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(441, 311);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(84, 19);
            this.metroLabel9.TabIndex = 32;
            this.metroLabel9.Text = "Date of birth";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(439, 249);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(63, 19);
            this.metroLabel4.TabIndex = 18;
            this.metroLabel4.Text = "Password";
            // 
            // role
            // 
            this.role.DisplayFocus = true;
            this.role.FormattingEnabled = true;
            this.role.ItemHeight = 23;
            this.role.Items.AddRange(new object[] {
            "Manager",
            "Front Office Clerk"});
            this.role.Location = new System.Drawing.Point(878, 333);
            this.role.Name = "role";
            this.role.Size = new System.Drawing.Size(192, 29);
            this.role.TabIndex = 22;
            this.role.UseSelectable = true;
            // 
            // userPassword
            // 
            // 
            // 
            // 
            this.userPassword.CustomButton.Image = null;
            this.userPassword.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPassword.CustomButton.Name = "";
            this.userPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPassword.CustomButton.TabIndex = 1;
            this.userPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPassword.CustomButton.UseSelectable = true;
            this.userPassword.CustomButton.Visible = false;
            this.userPassword.Lines = new string[0];
            this.userPassword.Location = new System.Drawing.Point(440, 271);
            this.userPassword.MaxLength = 32767;
            this.userPassword.Name = "userPassword";
            this.userPassword.PasswordChar = '\0';
            this.userPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPassword.SelectedText = "";
            this.userPassword.SelectionLength = 0;
            this.userPassword.SelectionStart = 0;
            this.userPassword.ShortcutsEnabled = true;
            this.userPassword.Size = new System.Drawing.Size(191, 23);
            this.userPassword.TabIndex = 17;
            this.userPassword.UseSelectable = true;
            this.userPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // userAddress
            // 
            // 
            // 
            // 
            this.userAddress.CustomButton.Image = null;
            this.userAddress.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userAddress.CustomButton.Name = "";
            this.userAddress.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userAddress.CustomButton.TabIndex = 1;
            this.userAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userAddress.CustomButton.UseSelectable = true;
            this.userAddress.CustomButton.Visible = false;
            this.userAddress.Lines = new string[0];
            this.userAddress.Location = new System.Drawing.Point(10, 333);
            this.userAddress.MaxLength = 32767;
            this.userAddress.Name = "userAddress";
            this.userAddress.PasswordChar = '\0';
            this.userAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userAddress.SelectedText = "";
            this.userAddress.SelectionLength = 0;
            this.userAddress.SelectionStart = 0;
            this.userAddress.ShortcutsEnabled = true;
            this.userAddress.Size = new System.Drawing.Size(191, 23);
            this.userAddress.TabIndex = 27;
            this.userAddress.UseSelectable = true;
            this.userAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(227, 249);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 16;
            this.metroLabel3.Text = "Email";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(9, 311);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(56, 19);
            this.metroLabel7.TabIndex = 28;
            this.metroLabel7.Text = "Address";
            // 
            // userEmail
            // 
            // 
            // 
            // 
            this.userEmail.CustomButton.Image = null;
            this.userEmail.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userEmail.CustomButton.Name = "";
            this.userEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userEmail.CustomButton.TabIndex = 1;
            this.userEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userEmail.CustomButton.UseSelectable = true;
            this.userEmail.CustomButton.Visible = false;
            this.userEmail.Lines = new string[0];
            this.userEmail.Location = new System.Drawing.Point(228, 271);
            this.userEmail.MaxLength = 32767;
            this.userEmail.Name = "userEmail";
            this.userEmail.PasswordChar = '\0';
            this.userEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userEmail.SelectedText = "";
            this.userEmail.SelectionLength = 0;
            this.userEmail.SelectionStart = 0;
            this.userEmail.ShortcutsEnabled = true;
            this.userEmail.Size = new System.Drawing.Size(191, 23);
            this.userEmail.TabIndex = 15;
            this.userEmail.UseSelectable = true;
            this.userEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(227, 311);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(99, 19);
            this.metroLabel8.TabIndex = 30;
            this.metroLabel8.Text = "Phone Number";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(8, 249);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(75, 19);
            this.metroLabel2.TabIndex = 14;
            this.metroLabel2.Text = "User Name";
            // 
            // userPhone
            // 
            // 
            // 
            // 
            this.userPhone.CustomButton.Image = null;
            this.userPhone.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPhone.CustomButton.Name = "";
            this.userPhone.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPhone.CustomButton.TabIndex = 1;
            this.userPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPhone.CustomButton.UseSelectable = true;
            this.userPhone.CustomButton.Visible = false;
            this.userPhone.Lines = new string[0];
            this.userPhone.Location = new System.Drawing.Point(228, 333);
            this.userPhone.MaxLength = 32767;
            this.userPhone.Name = "userPhone";
            this.userPhone.PasswordChar = '\0';
            this.userPhone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPhone.SelectedText = "";
            this.userPhone.SelectionLength = 0;
            this.userPhone.SelectionStart = 0;
            this.userPhone.ShortcutsEnabled = true;
            this.userPhone.Size = new System.Drawing.Size(191, 23);
            this.userPhone.TabIndex = 29;
            this.userPhone.UseSelectable = true;
            this.userPhone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPhone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // userName
            // 
            // 
            // 
            // 
            this.userName.CustomButton.Image = null;
            this.userName.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userName.CustomButton.Name = "";
            this.userName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userName.CustomButton.TabIndex = 1;
            this.userName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userName.CustomButton.UseSelectable = true;
            this.userName.CustomButton.Visible = false;
            this.userName.Lines = new string[0];
            this.userName.Location = new System.Drawing.Point(9, 271);
            this.userName.MaxLength = 32767;
            this.userName.Name = "userName";
            this.userName.PasswordChar = '\0';
            this.userName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userName.SelectedText = "";
            this.userName.SelectionLength = 0;
            this.userName.SelectionStart = 0;
            this.userName.ShortcutsEnabled = true;
            this.userName.Size = new System.Drawing.Size(191, 23);
            this.userName.TabIndex = 13;
            this.userName.UseSelectable = true;
            this.userName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // createUserButton
            // 
            this.createUserButton.Location = new System.Drawing.Point(10, 379);
            this.createUserButton.Name = "createUserButton";
            this.createUserButton.Size = new System.Drawing.Size(193, 23);
            this.createUserButton.TabIndex = 2;
            this.createUserButton.Text = "Create New User";
            this.createUserButton.UseSelectable = true;
            this.createUserButton.Click += new System.EventHandler(this.createUserButton_Click);
            // 
            // userListGrid
            // 
            this.userListGrid.AllowUserToAddRows = false;
            this.userListGrid.AllowUserToDeleteRows = false;
            this.userListGrid.AllowUserToResizeColumns = false;
            this.userListGrid.AllowUserToResizeRows = false;
            this.userListGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.userListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userListGrid.Location = new System.Drawing.Point(3, 18);
            this.userListGrid.Name = "userListGrid";
            this.userListGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.userListGrid.Size = new System.Drawing.Size(1067, 206);
            this.userListGrid.TabIndex = 7;
            this.userListGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.userListGrid_CellContentClick);
            // 
            // userEditButton
            // 
            this.userEditButton.Location = new System.Drawing.Point(229, 379);
            this.userEditButton.Name = "userEditButton";
            this.userEditButton.Size = new System.Drawing.Size(194, 23);
            this.userEditButton.TabIndex = 3;
            this.userEditButton.Text = "Edit User";
            this.userEditButton.UseSelectable = true;
            this.userEditButton.Click += new System.EventHandler(this.userEditButton_Click);
            // 
            // userDeleteButton
            // 
            this.userDeleteButton.Location = new System.Drawing.Point(656, 379);
            this.userDeleteButton.Name = "userDeleteButton";
            this.userDeleteButton.Size = new System.Drawing.Size(194, 23);
            this.userDeleteButton.TabIndex = 4;
            this.userDeleteButton.Text = "Delete User";
            this.userDeleteButton.UseSelectable = true;
            this.userDeleteButton.Click += new System.EventHandler(this.userDeleteButton_Click);
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroButton1);
            this.metroTabPage2.Controls.Add(this.btnSaveTreatment);
            this.metroTabPage2.Controls.Add(this.treatmentAmount);
            this.metroTabPage2.Controls.Add(this.metroLabel12);
            this.metroTabPage2.Controls.Add(this.treatmentName);
            this.metroTabPage2.Controls.Add(this.metroLabel11);
            this.metroTabPage2.Controls.Add(this.dataGridViewTreatments);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(1083, 424);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Manage Treatments";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            this.metroTabPage2.Click += new System.EventHandler(this.metroTabPage2_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(3, 338);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(123, 23);
            this.metroButton1.TabIndex = 8;
            this.metroButton1.Text = "Delete Treatment";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // btnSaveTreatment
            // 
            this.btnSaveTreatment.Location = new System.Drawing.Point(574, 98);
            this.btnSaveTreatment.Name = "btnSaveTreatment";
            this.btnSaveTreatment.Size = new System.Drawing.Size(190, 23);
            this.btnSaveTreatment.TabIndex = 7;
            this.btnSaveTreatment.Text = "Save Treatment";
            this.btnSaveTreatment.UseSelectable = true;
            this.btnSaveTreatment.Click += new System.EventHandler(this.btnSaveTreatment_Click);
            // 
            // treatmentAmount
            // 
            // 
            // 
            // 
            this.treatmentAmount.CustomButton.Image = null;
            this.treatmentAmount.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.treatmentAmount.CustomButton.Name = "";
            this.treatmentAmount.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.treatmentAmount.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.treatmentAmount.CustomButton.TabIndex = 1;
            this.treatmentAmount.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.treatmentAmount.CustomButton.UseSelectable = true;
            this.treatmentAmount.CustomButton.Visible = false;
            this.treatmentAmount.Lines = new string[0];
            this.treatmentAmount.Location = new System.Drawing.Point(574, 57);
            this.treatmentAmount.MaxLength = 32767;
            this.treatmentAmount.Name = "treatmentAmount";
            this.treatmentAmount.PasswordChar = '\0';
            this.treatmentAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.treatmentAmount.SelectedText = "";
            this.treatmentAmount.SelectionLength = 0;
            this.treatmentAmount.SelectionStart = 0;
            this.treatmentAmount.ShortcutsEnabled = true;
            this.treatmentAmount.Size = new System.Drawing.Size(190, 23);
            this.treatmentAmount.TabIndex = 6;
            this.treatmentAmount.UseSelectable = true;
            this.treatmentAmount.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.treatmentAmount.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.treatmentAmount.Click += new System.EventHandler(this.metroTextBox1_Click);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(450, 57);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(118, 19);
            this.metroLabel12.TabIndex = 5;
            this.metroLabel12.Text = "Treatment Amount";
            // 
            // treatmentName
            // 
            // 
            // 
            // 
            this.treatmentName.CustomButton.Image = null;
            this.treatmentName.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.treatmentName.CustomButton.Name = "";
            this.treatmentName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.treatmentName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.treatmentName.CustomButton.TabIndex = 1;
            this.treatmentName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.treatmentName.CustomButton.UseSelectable = true;
            this.treatmentName.CustomButton.Visible = false;
            this.treatmentName.Lines = new string[0];
            this.treatmentName.Location = new System.Drawing.Point(574, 15);
            this.treatmentName.MaxLength = 32767;
            this.treatmentName.Name = "treatmentName";
            this.treatmentName.PasswordChar = '\0';
            this.treatmentName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.treatmentName.SelectedText = "";
            this.treatmentName.SelectionLength = 0;
            this.treatmentName.SelectionStart = 0;
            this.treatmentName.ShortcutsEnabled = true;
            this.treatmentName.Size = new System.Drawing.Size(190, 23);
            this.treatmentName.TabIndex = 4;
            this.treatmentName.UseSelectable = true;
            this.treatmentName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.treatmentName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(450, 15);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(107, 19);
            this.metroLabel11.TabIndex = 3;
            this.metroLabel11.Text = "Treatment Name";
            // 
            // dataGridViewTreatments
            // 
            this.dataGridViewTreatments.AllowUserToAddRows = false;
            this.dataGridViewTreatments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTreatments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTreatments.Location = new System.Drawing.Point(0, 3);
            this.dataGridViewTreatments.Name = "dataGridViewTreatments";
            this.dataGridViewTreatments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTreatments.Size = new System.Drawing.Size(424, 329);
            this.dataGridViewTreatments.TabIndex = 2;
            this.dataGridViewTreatments.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTreatments_CellContentClick);
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.metroLabel14);
            this.metroTabPage3.Controls.Add(this.totalIncomeLabel);
            this.metroTabPage3.Controls.Add(this.gridCompletedAppointments);
            this.metroTabPage3.Controls.Add(this.metroButton2);
            this.metroTabPage3.Controls.Add(this.metroLabel13);
            this.metroTabPage3.Controls.Add(this.doctorsComboBox);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(1083, 424);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Income Report";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(3, 11);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(87, 19);
            this.metroLabel13.TabIndex = 11;
            this.metroLabel13.Text = "Select Doctor";
            // 
            // doctorsComboBox
            // 
            this.doctorsComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.doctorsComboBox.FormattingEnabled = true;
            this.doctorsComboBox.ItemHeight = 19;
            this.doctorsComboBox.Location = new System.Drawing.Point(3, 33);
            this.doctorsComboBox.Name = "doctorsComboBox";
            this.doctorsComboBox.Size = new System.Drawing.Size(196, 25);
            this.doctorsComboBox.TabIndex = 10;
            this.doctorsComboBox.UseSelectable = true;
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton2.Location = new System.Drawing.Point(206, 33);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(145, 25);
            this.metroButton2.TabIndex = 12;
            this.metroButton2.Text = "Generate Report";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // gridCompletedAppointments
            // 
            this.gridCompletedAppointments.AllowUserToAddRows = false;
            this.gridCompletedAppointments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridCompletedAppointments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCompletedAppointments.Location = new System.Drawing.Point(3, 64);
            this.gridCompletedAppointments.Name = "gridCompletedAppointments";
            this.gridCompletedAppointments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCompletedAppointments.Size = new System.Drawing.Size(1077, 187);
            this.gridCompletedAppointments.TabIndex = 13;
            // 
            // totalIncomeLabel
            // 
            this.totalIncomeLabel.AutoSize = true;
            this.totalIncomeLabel.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.totalIncomeLabel.Location = new System.Drawing.Point(111, 281);
            this.totalIncomeLabel.Name = "totalIncomeLabel";
            this.totalIncomeLabel.Size = new System.Drawing.Size(53, 25);
            this.totalIncomeLabel.TabIndex = 14;
            this.totalIncomeLabel.Text = "0 LKR";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(7, 281);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(83, 19);
            this.metroLabel14.TabIndex = 15;
            this.metroLabel14.Text = "Total Income";
            // 
            // AdministratorMainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1114, 464);
            this.Controls.Add(this.ds);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "AdministratorMainView";
            this.Text = "Admin Home";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdministratorMainView_FormClosed);
            this.Load += new System.EventHandler(this.AdministratorMainView_Load);
            this.metroContextMenu1.ResumeLayout(false);
            this.ds.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userListGrid)).EndInit();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTreatments)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompletedAppointments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.ToolStripMenuItem addNewUserToolStripMenuItem;
        private MetroFramework.Controls.MetroTabControl ds;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroButton createUserButton;
        private MetroFramework.Controls.MetroButton userDeleteButton;
        private MetroFramework.Controls.MetroButton userEditButton;
        private System.Windows.Forms.DataGridView userListGrid;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroComboBox role;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox userPasswordRep;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox userPassword;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox userEmail;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox userName;
        private MetroFramework.Controls.MetroButton updateUserButton;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox userAddress;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox userNic;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox userPhone;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroDateTime userDateOfBirth;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private System.Windows.Forms.DataGridView dataGridViewTreatments;
        private MetroFramework.Controls.MetroTextBox treatmentAmount;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroTextBox treatmentName;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroButton btnSaveTreatment;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox userExpYears;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroComboBox doctorsComboBox;
        private MetroFramework.Controls.MetroButton metroButton2;
        private System.Windows.Forms.DataGridView gridCompletedAppointments;
        private MetroFramework.Controls.MetroLabel totalIncomeLabel;
        private MetroFramework.Controls.MetroLabel metroLabel14;
    }
}