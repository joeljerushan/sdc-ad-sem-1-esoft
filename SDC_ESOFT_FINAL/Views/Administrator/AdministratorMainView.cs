﻿using SDC_ESOFT_FINAL.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//roles
//1 - manager
//2 - front officer
//3 - doctor 

namespace SDC_ESOFT_FINAL.Views.Administrator
{
    public partial class AdministratorMainView : Form
    {

        User user = new User();
        Treatments treatments = new Treatments();
        Appointment appointment = new Appointment();

        public AdministratorMainView()
        {
            InitializeComponent();

            //on load fill userListGrid
            user.ListUsers(userListGrid);
            treatments.List(dataGridViewTreatments);

            role.SelectedIndex = 0;
            userExpYears.SelectedIndex = 6;

            //get doctors to combo box
            user.ComboBoxUsers(doctorsComboBox, 3);
        }

        private void AdministratorMainView_Load(object sender, EventArgs e)
        {

        }

        private void AdministratorMainView_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void createUserButton_Click(object sender, EventArgs e)
        {

            string name = userName.Text;
            string email = userEmail.Text;
            string password = userPassword.Text;
            string password_repeat = userPasswordRep.Text;
            string nic = userNic.Text;
            string address = userAddress.Text;
            string phone = userPhone.Text;
            string date_of_birth = userDateOfBirth.Text;
            int exp_years = userExpYears.SelectedIndex + 1;
            int role_id = role.SelectedIndex + 1;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name Required");
            }
            else if (string.IsNullOrEmpty(email))
            {
                MessageBox.Show("Email Required");
            }
            else if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password Required");
            }
            else
            {
                if (password == password_repeat)
                {
                    user.USER_NAME = name;
                    user.USER_EMAIL = email;
                    user.USER_PASSWORD = password;
                    user.USER_NIC = nic;
                    user.USER_ADDRESS = address;
                    user.USER_PHONE = phone;
                    user.USER_DATE_OF_BIRTH = date_of_birth;
                    user.USER_EXP_YEARS = exp_years;
                    user.ACCESS_ROLE = role_id;

                    //creating user function
                    user.CreateUser();

                    //clear current values in text box
                    userName.Text = "";
                    userEmail.Text = "";
                    userPassword.Text = "";
                    userPasswordRep.Text = "";
                    role.SelectedIndex = 0;
                    userExpYears.SelectedIndex = 6;
                    userNic.Text = "";
                    userAddress.Text = "";
                    userPhone.Text = "";
                    userDateOfBirth.Text = "";

                    //reload data table
                    user.ListUsers(userListGrid);

                    //show success message
                    MessageBox.Show("User Created");
                }
                else
                {
                    MessageBox.Show("Password Don't Match");
                }
            }


        }


        private void userDeleteButton_Click(object sender, EventArgs e)
        {
            // get currently focus row id and set it to a string 
            string DELETE_ID = userListGrid.SelectedRows[0].Cells[0].Value.ToString();

            //set user class user id 
            user.USER_ID = DELETE_ID;

            // call delete user function
            user.DeleteUser();
           
            //reload data table
            user.ListUsers(userListGrid);

            //show success message 
            MessageBox.Show("User Removed");
        }

        private void userEditButton_Click(object sender, EventArgs e)
        {
            //getting currently selected row values and save them on string 
            string USER_ID = userListGrid.SelectedRows[0].Cells[0].Value.ToString();
            string USER_NAME = userListGrid.SelectedRows[0].Cells[1].Value.ToString();
            string USER_EMAIL = userListGrid.SelectedRows[0].Cells[2].Value.ToString();
            string USER_PASSWORD = userListGrid.SelectedRows[0].Cells[3].Value.ToString();
            string USER_ROLE = userListGrid.SelectedRows[0].Cells[4].Value.ToString();
            string USER_NIC = userListGrid.SelectedRows[0].Cells[5].Value.ToString();
            string USER_ADDRESS = userListGrid.SelectedRows[0].Cells[6].Value.ToString();
            string USER_PHONE = userListGrid.SelectedRows[0].Cells[7].Value.ToString();
            string USER_DATE_OF_BIRTH = userListGrid.SelectedRows[0].Cells[8].Value.ToString();
            string USER_EXP_YEARS = userListGrid.SelectedRows[0].Cells[9].Value.ToString();

            //convert data to integer and reduce -1 to show in ui 
            int exp_years = Convert.ToInt32(USER_EXP_YEARS) - 1;
            int role_id = Convert.ToInt32(USER_ROLE) - 1;

            //assign string values to ui elements for edit 
            userName.Text = USER_NAME;
            userEmail.Text = USER_EMAIL;
            userPassword.Text = USER_PASSWORD;
            userPasswordRep.Text = USER_PASSWORD;
            userExpYears.SelectedIndex = 1;
            userNic.Text = USER_NIC;
            userAddress.Text = USER_ADDRESS;
            userPhone.Text = USER_PHONE;
            userDateOfBirth.Text = USER_DATE_OF_BIRTH;
            userExpYears.SelectedIndex = exp_years;
            role.SelectedIndex = role_id;
        }

        private void updateUserButton_Click(object sender, EventArgs e)
        {
            //getting values from ui elements 
            string name = userName.Text;
            string email = userEmail.Text;
            string password = userPassword.Text;
            string password_repeat = userPasswordRep.Text;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name Required");
            }
            else if (string.IsNullOrEmpty(email))
            {
                MessageBox.Show("Email Required");
            }
            else if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password Required");
            }
            else
            {
                // if both passwords matched 
                if (password == password_repeat)
                {
                    //user id from selected row 
                    user.USER_ID = userListGrid.SelectedRows[0].Cells[0].Value.ToString();
                    user.USER_NAME = name;
                    user.USER_EMAIL = email;
                    user.USER_PASSWORD = password;

                    //creating user function 
                    user.UpdateUser();

                    //clear current values in text box 
                    userName.Text = "";
                    userEmail.Text = "";
                    userPassword.Text = "";
                    userPasswordRep.Text = "";

                    //reload data table
                    user.ListUsers(userListGrid);

                }
                else
                {
                    MessageBox.Show("Password Don't Match");
                }
            }
        }

        private void metroTabPage1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridViewTreatments_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroTabPage2_Click(object sender, EventArgs e)
        {
            treatments.List(dataGridViewTreatments);
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnSaveTreatment_Click(object sender, EventArgs e)
        {
            //getting values from ui elements 
            string name = treatmentName.Text;
            string amount = treatmentAmount.Text;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Treatment Name Required");
            }
            else if (string.IsNullOrEmpty(amount))
            {
                MessageBox.Show("Treatment Amount Required");
            } else
            {
                treatments.TREATMENT_NAME = name;
                treatments.TREATMENT_AMOUNT = Convert.ToInt32(amount);

                treatments.Create();

                treatmentName.Text = "";
                treatmentAmount.Text = "";

                treatments.List(dataGridViewTreatments);
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            // get currently focus row id and set it to a string 
            string DELETE_ID = dataGridViewTreatments.SelectedRows[0].Cells[0].Value.ToString();

            //set user class user id 
            treatments.TREATMENT_ID = DELETE_ID;

            // call delete user function
            treatments.Delete();

            //reload data table
            treatments.List(dataGridViewTreatments);

            //show success message 
            MessageBox.Show("Treatment Removed");
        }

        private void userListGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            //selected doctor id
            string SELECTED_DOCTOR = doctorsComboBox.SelectedValue.ToString();

            totalIncomeLabel.Text = "";

            //get completed appointments 
            appointment.ListCompletedByDoctorId(gridCompletedAppointments, Convert.ToInt32(SELECTED_DOCTOR), totalIncomeLabel);


        }
    }
}
