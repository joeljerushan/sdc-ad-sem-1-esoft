﻿namespace SDC_ESOFT_FINAL.Views.Doctors
{
    partial class DoctorsAdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Treatments = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.completeTreatment = new MetroFramework.Controls.MetroButton();
            this.comboTreatments = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.treatmentName = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.selectUserBtn = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridViewTreatmentHistory = new System.Windows.Forms.DataGridView();
            this.btnViewHistory = new MetroFramework.Controls.MetroButton();
            this.dataGridViewAppointments = new System.Windows.Forms.DataGridView();
            this.Treatments.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTreatmentHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAppointments)).BeginInit();
            this.SuspendLayout();
            // 
            // Treatments
            // 
            this.Treatments.Controls.Add(this.metroTabPage1);
            this.Treatments.Location = new System.Drawing.Point(12, 12);
            this.Treatments.Name = "Treatments";
            this.Treatments.SelectedIndex = 0;
            this.Treatments.Size = new System.Drawing.Size(953, 506);
            this.Treatments.TabIndex = 0;
            this.Treatments.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.completeTreatment);
            this.metroTabPage1.Controls.Add(this.comboTreatments);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.treatmentName);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.selectUserBtn);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.dataGridViewTreatmentHistory);
            this.metroTabPage1.Controls.Add(this.btnViewHistory);
            this.metroTabPage1.Controls.Add(this.dataGridViewAppointments);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(945, 464);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Appointments and Treatments";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // completeTreatment
            // 
            this.completeTreatment.Location = new System.Drawing.Point(134, 423);
            this.completeTreatment.Name = "completeTreatment";
            this.completeTreatment.Size = new System.Drawing.Size(232, 23);
            this.completeTreatment.TabIndex = 13;
            this.completeTreatment.Text = "Complete Treatment";
            this.completeTreatment.UseSelectable = true;
            this.completeTreatment.Click += new System.EventHandler(this.completeTreatment_Click);
            // 
            // comboTreatments
            // 
            this.comboTreatments.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.comboTreatments.FormattingEnabled = true;
            this.comboTreatments.ItemHeight = 19;
            this.comboTreatments.Location = new System.Drawing.Point(134, 383);
            this.comboTreatments.Name = "comboTreatments";
            this.comboTreatments.Size = new System.Drawing.Size(232, 25);
            this.comboTreatments.TabIndex = 12;
            this.comboTreatments.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(0, 383);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(103, 19);
            this.metroLabel5.TabIndex = 11;
            this.metroLabel5.Text = "Treatment Type";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(0, 354);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(92, 19);
            this.metroLabel4.TabIndex = 10;
            this.metroLabel4.Text = "Patient Name";
            // 
            // treatmentName
            // 
            this.treatmentName.AutoSize = true;
            this.treatmentName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.treatmentName.Location = new System.Drawing.Point(134, 354);
            this.treatmentName.Name = "treatmentName";
            this.treatmentName.Size = new System.Drawing.Size(21, 19);
            this.treatmentName.TabIndex = 9;
            this.treatmentName.Text = "   ";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.Location = new System.Drawing.Point(-4, 312);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(123, 25);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "Add Treatment";
            // 
            // selectUserBtn
            // 
            this.selectUserBtn.Location = new System.Drawing.Point(204, 266);
            this.selectUserBtn.Name = "selectUserBtn";
            this.selectUserBtn.Size = new System.Drawing.Size(199, 23);
            this.selectUserBtn.TabIndex = 7;
            this.selectUserBtn.Text = "Add Treatment";
            this.selectUserBtn.UseSelectable = true;
            this.selectUserBtn.Click += new System.EventHandler(this.selectUserBtn_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.Location = new System.Drawing.Point(-4, 14);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(176, 25);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Today\'s Appointments";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(427, 15);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(145, 25);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Treatment History";
            // 
            // dataGridViewTreatmentHistory
            // 
            this.dataGridViewTreatmentHistory.AllowUserToAddRows = false;
            this.dataGridViewTreatmentHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewTreatmentHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTreatmentHistory.Location = new System.Drawing.Point(427, 43);
            this.dataGridViewTreatmentHistory.Name = "dataGridViewTreatmentHistory";
            this.dataGridViewTreatmentHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTreatmentHistory.Size = new System.Drawing.Size(522, 217);
            this.dataGridViewTreatmentHistory.TabIndex = 4;
            this.dataGridViewTreatmentHistory.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTreatmentHistory_CellContentClick);
            // 
            // btnViewHistory
            // 
            this.btnViewHistory.Location = new System.Drawing.Point(1, 266);
            this.btnViewHistory.Name = "btnViewHistory";
            this.btnViewHistory.Size = new System.Drawing.Size(197, 23);
            this.btnViewHistory.TabIndex = 3;
            this.btnViewHistory.Text = "View Treatment History";
            this.btnViewHistory.UseSelectable = true;
            this.btnViewHistory.Click += new System.EventHandler(this.btnViewHistory_Click);
            // 
            // dataGridViewAppointments
            // 
            this.dataGridViewAppointments.AllowUserToAddRows = false;
            this.dataGridViewAppointments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAppointments.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAppointments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAppointments.Location = new System.Drawing.Point(0, 43);
            this.dataGridViewAppointments.Name = "dataGridViewAppointments";
            this.dataGridViewAppointments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAppointments.Size = new System.Drawing.Size(403, 217);
            this.dataGridViewAppointments.TabIndex = 2;
            this.dataGridViewAppointments.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAppointments_CellContentClick);
            // 
            // DoctorsAdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(977, 522);
            this.Controls.Add(this.Treatments);
            this.Name = "DoctorsAdminPanel";
            this.Text = "Doctors Panel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DoctorsAdminPanel_FormClosed);
            this.Load += new System.EventHandler(this.DoctorsAdminPanel_Load);
            this.Treatments.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTreatmentHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAppointments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl Treatments;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private System.Windows.Forms.DataGridView dataGridViewAppointments;
        private MetroFramework.Controls.MetroButton btnViewHistory;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridViewTreatmentHistory;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton selectUserBtn;
        private MetroFramework.Controls.MetroLabel treatmentName;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox comboTreatments;
        private MetroFramework.Controls.MetroButton completeTreatment;
    }
}