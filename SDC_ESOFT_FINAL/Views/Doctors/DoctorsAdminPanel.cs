﻿using SDC_ESOFT_FINAL.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDC_ESOFT_FINAL.Views.Doctors
{
    public partial class DoctorsAdminPanel : Form
    {

        Appointment appointments = new Appointment();
        Treatments treatments = new Treatments();
        AppointmentTreatment appointmentTreatment = new AppointmentTreatment();

        public DoctorsAdminPanel()
        {
            InitializeComponent();
        }

        private void DoctorsAdminPanel_Load(object sender, EventArgs e)
        {

            //list appointments by current date
            appointments.ListByCurrentDate(dataGridViewAppointments);

            //load treatments
            treatments.ComboBox(comboTreatments);
            
        }

        private void btnViewHistory_Click(object sender, EventArgs e)
        {
            if(Convert.ToInt32(dataGridViewAppointments.Rows.Count) == 0)
            {
                MessageBox.Show("No Appointments assigned for you Today!");
            } else
            {
                //select patient id from row
                string PATIENT_ID = dataGridViewAppointments.SelectedRows[0].Cells[1].Value.ToString();

                //set patient ID
                appointmentTreatment.PATIENT_ID = Convert.ToInt32(PATIENT_ID);

                //load selected patient's treatment history
                appointmentTreatment.List(dataGridViewTreatmentHistory);
            }

        }

        private void DoctorsAdminPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void selectUserBtn_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(dataGridViewAppointments.Rows.Count) == 0)
            {
                MessageBox.Show("No Appointments assigned for you Today!");
            } else
            {
                //get patient id from selected row 
                string PATIENT_ID = dataGridViewAppointments.SelectedRows[0].Cells[1].Value.ToString();

                //set patient ID
                appointmentTreatment.PATIENT_ID = Convert.ToInt32(PATIENT_ID);

                //get patient name from selected row 
                string PATIENT_NAME = dataGridViewAppointments.SelectedRows[0].Cells[2].Value.ToString();

                //assign name on lable to show
                treatmentName.Text = PATIENT_NAME;

                //select appointment for to assign 
                string APPOINTMENT_ID = dataGridViewAppointments.SelectedRows[0].Cells[0].Value.ToString();

                //assign appointment ID for treatment complete
                appointmentTreatment.SELECTED_APPOINTMENT_ID = Convert.ToInt32(APPOINTMENT_ID);
            }

        }

        private void completeTreatment_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(dataGridViewAppointments.Rows.Count) == 0)
            {
                MessageBox.Show("No Appointments assigned for you Today!");
            }
            else
            {
                //select treatment id from Treatment Combo Box
                string TREATMENT_ID = comboTreatments.SelectedValue.ToString();

                //asign tretment id
                appointmentTreatment.TREATMENT_ID = Convert.ToInt32(TREATMENT_ID);

                appointmentTreatment.CreateAppointmentTreatment();

                //load selected patient's treatment history
                appointmentTreatment.List(dataGridViewTreatmentHistory);

                //list updated appoinments
                appointments.ListByCurrentDate(dataGridViewAppointments);

                MessageBox.Show("Treatment Completed");
            }
        }

        private void dataGridViewTreatmentHistory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridViewAppointments_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
