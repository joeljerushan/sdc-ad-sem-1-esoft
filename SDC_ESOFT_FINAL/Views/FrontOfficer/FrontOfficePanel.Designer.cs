﻿namespace SDC_ESOFT_FINAL.Views.FrontOfficer
{
    partial class FrontOfficePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.dataGridViewPatient = new System.Windows.Forms.DataGridView();
            this.updateUserButton = new MetroFramework.Controls.MetroButton();
            this.createUserButton = new MetroFramework.Controls.MetroButton();
            this.userEditButton = new MetroFramework.Controls.MetroButton();
            this.userDeleteButton = new MetroFramework.Controls.MetroButton();
            this.contact = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.address = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.nic = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.name = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.doctorsComboBox = new MetroFramework.Controls.MetroComboBox();
            this.btnCreateAppointment = new MetroFramework.Controls.MetroButton();
            this.btnSearch = new MetroFramework.Controls.MetroButton();
            this.searchNic = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.dataGridViewSearchResult = new System.Windows.Forms.DataGridView();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.btnPrintInvoice = new MetroFramework.Controls.MetroButton();
            this.gridCompletedAppointments = new System.Windows.Forms.DataGridView();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatient)).BeginInit();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearchResult)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCompletedAppointments)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Location = new System.Drawing.Point(12, 12);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 2;
            this.metroTabControl1.Size = new System.Drawing.Size(883, 449);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.dataGridViewPatient);
            this.metroTabPage1.Controls.Add(this.updateUserButton);
            this.metroTabPage1.Controls.Add(this.createUserButton);
            this.metroTabPage1.Controls.Add(this.userEditButton);
            this.metroTabPage1.Controls.Add(this.userDeleteButton);
            this.metroTabPage1.Controls.Add(this.contact);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.address);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.nic);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.name);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(875, 407);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Patient";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // dataGridViewPatient
            // 
            this.dataGridViewPatient.AllowUserToAddRows = false;
            this.dataGridViewPatient.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPatient.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewPatient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPatient.Location = new System.Drawing.Point(0, 3);
            this.dataGridViewPatient.Name = "dataGridViewPatient";
            this.dataGridViewPatient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPatient.Size = new System.Drawing.Size(872, 298);
            this.dataGridViewPatient.TabIndex = 29;
            // 
            // updateUserButton
            // 
            this.updateUserButton.Location = new System.Drawing.Point(443, 364);
            this.updateUserButton.Name = "updateUserButton";
            this.updateUserButton.Size = new System.Drawing.Size(191, 23);
            this.updateUserButton.TabIndex = 28;
            this.updateUserButton.Text = "Update Patient";
            this.updateUserButton.UseSelectable = true;
            // 
            // createUserButton
            // 
            this.createUserButton.Location = new System.Drawing.Point(3, 364);
            this.createUserButton.Name = "createUserButton";
            this.createUserButton.Size = new System.Drawing.Size(191, 23);
            this.createUserButton.TabIndex = 25;
            this.createUserButton.Text = "Create New Patient";
            this.createUserButton.UseSelectable = true;
            this.createUserButton.Click += new System.EventHandler(this.createUserButton_Click_1);
            // 
            // userEditButton
            // 
            this.userEditButton.Location = new System.Drawing.Point(225, 364);
            this.userEditButton.Name = "userEditButton";
            this.userEditButton.Size = new System.Drawing.Size(191, 23);
            this.userEditButton.TabIndex = 26;
            this.userEditButton.Text = "Edit Patient";
            this.userEditButton.UseSelectable = true;
            // 
            // userDeleteButton
            // 
            this.userDeleteButton.Location = new System.Drawing.Point(659, 364);
            this.userDeleteButton.Name = "userDeleteButton";
            this.userDeleteButton.Size = new System.Drawing.Size(191, 23);
            this.userDeleteButton.TabIndex = 27;
            this.userDeleteButton.Text = "Delete Patient";
            this.userDeleteButton.UseSelectable = true;
            this.userDeleteButton.Click += new System.EventHandler(this.userDeleteButton_Click_1);
            // 
            // contact
            // 
            // 
            // 
            // 
            this.contact.CustomButton.Image = null;
            this.contact.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.contact.CustomButton.Name = "";
            this.contact.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.contact.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.contact.CustomButton.TabIndex = 1;
            this.contact.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.contact.CustomButton.UseSelectable = true;
            this.contact.CustomButton.Visible = false;
            this.contact.Lines = new string[0];
            this.contact.Location = new System.Drawing.Point(659, 335);
            this.contact.MaxLength = 32767;
            this.contact.Name = "contact";
            this.contact.PasswordChar = '\0';
            this.contact.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.contact.SelectedText = "";
            this.contact.SelectionLength = 0;
            this.contact.SelectionStart = 0;
            this.contact.ShortcutsEnabled = true;
            this.contact.Size = new System.Drawing.Size(191, 23);
            this.contact.TabIndex = 20;
            this.contact.UseSelectable = true;
            this.contact.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.contact.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(659, 313);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(54, 19);
            this.metroLabel4.TabIndex = 19;
            this.metroLabel4.Text = "Contact";
            // 
            // address
            // 
            // 
            // 
            // 
            this.address.CustomButton.Image = null;
            this.address.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.address.CustomButton.Name = "";
            this.address.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.address.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.address.CustomButton.TabIndex = 1;
            this.address.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.address.CustomButton.UseSelectable = true;
            this.address.CustomButton.Visible = false;
            this.address.Lines = new string[0];
            this.address.Location = new System.Drawing.Point(443, 335);
            this.address.MaxLength = 32767;
            this.address.Name = "address";
            this.address.PasswordChar = '\0';
            this.address.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.address.SelectedText = "";
            this.address.SelectionLength = 0;
            this.address.SelectionStart = 0;
            this.address.ShortcutsEnabled = true;
            this.address.Size = new System.Drawing.Size(191, 23);
            this.address.TabIndex = 18;
            this.address.UseSelectable = true;
            this.address.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.address.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(443, 313);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(56, 19);
            this.metroLabel3.TabIndex = 17;
            this.metroLabel3.Text = "Address";
            // 
            // nic
            // 
            // 
            // 
            // 
            this.nic.CustomButton.Image = null;
            this.nic.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.nic.CustomButton.Name = "";
            this.nic.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.nic.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.nic.CustomButton.TabIndex = 1;
            this.nic.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.nic.CustomButton.UseSelectable = true;
            this.nic.CustomButton.Visible = false;
            this.nic.Lines = new string[0];
            this.nic.Location = new System.Drawing.Point(225, 335);
            this.nic.MaxLength = 32767;
            this.nic.Name = "nic";
            this.nic.PasswordChar = '\0';
            this.nic.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.nic.SelectedText = "";
            this.nic.SelectionLength = 0;
            this.nic.SelectionStart = 0;
            this.nic.ShortcutsEnabled = true;
            this.nic.Size = new System.Drawing.Size(191, 23);
            this.nic.TabIndex = 16;
            this.nic.UseSelectable = true;
            this.nic.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.nic.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(225, 313);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(31, 19);
            this.metroLabel2.TabIndex = 15;
            this.metroLabel2.Text = "NIC";
            // 
            // name
            // 
            // 
            // 
            // 
            this.name.CustomButton.Image = null;
            this.name.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.name.CustomButton.Name = "";
            this.name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.name.CustomButton.TabIndex = 1;
            this.name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.name.CustomButton.UseSelectable = true;
            this.name.CustomButton.Visible = false;
            this.name.Lines = new string[0];
            this.name.Location = new System.Drawing.Point(3, 335);
            this.name.MaxLength = 32767;
            this.name.Name = "name";
            this.name.PasswordChar = '\0';
            this.name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.name.SelectedText = "";
            this.name.SelectionLength = 0;
            this.name.SelectionStart = 0;
            this.name.ShortcutsEnabled = true;
            this.name.Size = new System.Drawing.Size(191, 23);
            this.name.TabIndex = 14;
            this.name.UseSelectable = true;
            this.name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 313);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(88, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Patient Name";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroLabel6);
            this.metroTabPage2.Controls.Add(this.doctorsComboBox);
            this.metroTabPage2.Controls.Add(this.btnCreateAppointment);
            this.metroTabPage2.Controls.Add(this.btnSearch);
            this.metroTabPage2.Controls.Add(this.searchNic);
            this.metroTabPage2.Controls.Add(this.metroLabel5);
            this.metroTabPage2.Controls.Add(this.dataGridViewSearchResult);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(875, 407);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Appointments";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(371, 16);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(87, 19);
            this.metroLabel6.TabIndex = 9;
            this.metroLabel6.Text = "Select Doctor";
            // 
            // doctorsComboBox
            // 
            this.doctorsComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.doctorsComboBox.FormattingEnabled = true;
            this.doctorsComboBox.ItemHeight = 19;
            this.doctorsComboBox.Location = new System.Drawing.Point(371, 38);
            this.doctorsComboBox.Name = "doctorsComboBox";
            this.doctorsComboBox.Size = new System.Drawing.Size(196, 25);
            this.doctorsComboBox.TabIndex = 8;
            this.doctorsComboBox.UseSelectable = true;
            // 
            // btnCreateAppointment
            // 
            this.btnCreateAppointment.Location = new System.Drawing.Point(573, 38);
            this.btnCreateAppointment.Name = "btnCreateAppointment";
            this.btnCreateAppointment.Size = new System.Drawing.Size(140, 23);
            this.btnCreateAppointment.TabIndex = 7;
            this.btnCreateAppointment.Text = "Create Appointment";
            this.btnCreateAppointment.UseSelectable = true;
            this.btnCreateAppointment.Click += new System.EventHandler(this.btnCreateAppointment_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(208, 38);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 23);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search Patient";
            this.btnSearch.UseSelectable = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // searchNic
            // 
            // 
            // 
            // 
            this.searchNic.CustomButton.Image = null;
            this.searchNic.CustomButton.Location = new System.Drawing.Point(174, 1);
            this.searchNic.CustomButton.Name = "";
            this.searchNic.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.searchNic.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.searchNic.CustomButton.TabIndex = 1;
            this.searchNic.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.searchNic.CustomButton.UseSelectable = true;
            this.searchNic.CustomButton.Visible = false;
            this.searchNic.Lines = new string[0];
            this.searchNic.Location = new System.Drawing.Point(3, 38);
            this.searchNic.MaxLength = 32767;
            this.searchNic.Name = "searchNic";
            this.searchNic.PasswordChar = '\0';
            this.searchNic.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.searchNic.SelectedText = "";
            this.searchNic.SelectionLength = 0;
            this.searchNic.SelectionStart = 0;
            this.searchNic.ShortcutsEnabled = true;
            this.searchNic.Size = new System.Drawing.Size(196, 23);
            this.searchNic.TabIndex = 5;
            this.searchNic.UseSelectable = true;
            this.searchNic.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.searchNic.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 16);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(127, 19);
            this.metroLabel5.TabIndex = 3;
            this.metroLabel5.Text = "Patient NIC Number";
            // 
            // dataGridViewSearchResult
            // 
            this.dataGridViewSearchResult.AllowUserToAddRows = false;
            this.dataGridViewSearchResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSearchResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSearchResult.Location = new System.Drawing.Point(3, 85);
            this.dataGridViewSearchResult.Name = "dataGridViewSearchResult";
            this.dataGridViewSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSearchResult.Size = new System.Drawing.Size(869, 69);
            this.dataGridViewSearchResult.TabIndex = 2;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.btnPrintInvoice);
            this.metroTabPage3.Controls.Add(this.gridCompletedAppointments);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(875, 407);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Completed Appointments";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // btnPrintInvoice
            // 
            this.btnPrintInvoice.Location = new System.Drawing.Point(3, 196);
            this.btnPrintInvoice.Name = "btnPrintInvoice";
            this.btnPrintInvoice.Size = new System.Drawing.Size(147, 23);
            this.btnPrintInvoice.TabIndex = 3;
            this.btnPrintInvoice.Text = "Print Invoice";
            this.btnPrintInvoice.UseSelectable = true;
            this.btnPrintInvoice.Click += new System.EventHandler(this.btnPrintInvoice_Click);
            // 
            // gridCompletedAppointments
            // 
            this.gridCompletedAppointments.AllowUserToAddRows = false;
            this.gridCompletedAppointments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridCompletedAppointments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCompletedAppointments.Location = new System.Drawing.Point(0, 3);
            this.gridCompletedAppointments.Name = "gridCompletedAppointments";
            this.gridCompletedAppointments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridCompletedAppointments.Size = new System.Drawing.Size(872, 187);
            this.gridCompletedAppointments.TabIndex = 2;
            // 
            // FrontOfficePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(907, 468);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FrontOfficePanel";
            this.Text = " Front Office Panel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrontOfficePanel_FormClosed);
            this.Load += new System.EventHandler(this.FrontOfficePanel_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatient)).EndInit();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearchResult)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCompletedAppointments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox address;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox nic;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox name;
        private MetroFramework.Controls.MetroTextBox contact;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton updateUserButton;
        private MetroFramework.Controls.MetroButton createUserButton;
        private MetroFramework.Controls.MetroButton userEditButton;
        private MetroFramework.Controls.MetroButton userDeleteButton;
        private System.Windows.Forms.DataGridView dataGridViewPatient;
        private MetroFramework.Controls.MetroButton btnSearch;
        private MetroFramework.Controls.MetroTextBox searchNic;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.DataGridView dataGridViewSearchResult;
        private MetroFramework.Controls.MetroButton btnCreateAppointment;
        private MetroFramework.Controls.MetroComboBox doctorsComboBox;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private System.Windows.Forms.DataGridView gridCompletedAppointments;
        private MetroFramework.Controls.MetroButton btnPrintInvoice;
    }
}