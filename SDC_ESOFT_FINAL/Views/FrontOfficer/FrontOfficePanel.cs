﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SDC_ESOFT_FINAL.Classes;

namespace SDC_ESOFT_FINAL.Views.FrontOfficer
{
    public partial class FrontOfficePanel : Form
    {

        Patient patient = new Patient();
        Appointment appointment = new Appointment();
        User user = new User();

        public FrontOfficePanel()
        {
            InitializeComponent();
        }

        private void FrontOfficePanel_Load(object sender, EventArgs e)
        {
            //on load fill userListGrid
            patient.List(dataGridViewPatient);

            //get doctors to combo box
            user.ComboBoxUsers(doctorsComboBox, 3);

            //get completed appointments 
            appointment.ListCompleted(gridCompletedAppointments);

        }

        private void createUserButton_Click(object sender, EventArgs e)
        {

        }

        private void userEditButton_Click(object sender, EventArgs e)
        {

        }

        private void userDeleteButton_Click(object sender, EventArgs e)
        {

        }

        private void createUserButton_Click_1(object sender, EventArgs e)
        {
            string name = this.name.Text;
            string nic = this.nic.Text;
            string address = this.address.Text;
            string contact = this.contact.Text;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name Required");
            }
            else if (string.IsNullOrEmpty(nic))
            {
                MessageBox.Show("NIC Required");
            }
            else if (string.IsNullOrEmpty(address))
            {
                MessageBox.Show("Address Required");
            }
            else if (string.IsNullOrEmpty(contact))
            {
                MessageBox.Show("Contact Number Required");
            } else
            {
                patient.PATIENT_NAME = name;
                patient.PATIENT_NIC = nic;
                patient.PATIENT_ADDRESS = address;
                patient.PATIENT_PHONE = contact;

                //creating user function
                patient.Create();

                //clear current values in text box
                this.name.Text = "";
                this.nic.Text = "";
                this.address.Text = "";
                this.contact.Text = "";

                patient.List(dataGridViewPatient);

                //show success message
                MessageBox.Show("Patient Created");
            }
        }

        private void userDeleteButton_Click_1(object sender, EventArgs e)
        {
            // get currently focus row id and set it to a string 
            string DELETE_ID = dataGridViewPatient.SelectedRows[0].Cells[0].Value.ToString();

            //set user class user id 
            patient.PATIENT_ID = DELETE_ID;

            // call delete user function
            patient.Delete();

            //reload data table
            patient.List(dataGridViewPatient);

            //show success message 
            MessageBox.Show("Patient Removed");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string nic = this.searchNic.Text;

            //if nic field is not empty
            if (nic != "")
            {
                //assign patient nic on patient class
                patient.PATIENT_NIC = nic;

                //do fill data grid view
                patient.Search(dataGridViewSearchResult);
            }
            else
            {
                //if no inc given show valit nic number
                MessageBox.Show("Please Input valid NIC Number");
            }

        }

        private void FrontOfficePanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void btnCreateAppointment_Click(object sender, EventArgs e)
        {
            //check if search result has minimum one value and selected on patient from gridview
            if (Convert.ToInt32(dataGridViewSearchResult.Rows.Count) == 0)
            {   
                //if there is not patient selected show alert
                MessageBox.Show("Please Select One Patient");
            } else
            {
                //selected patient id 
                string SELECTED_PATIENT = dataGridViewSearchResult.SelectedRows[0].Cells[0].Value.ToString();

                //selected doctor id
                string SELECTED_DOCTOR = doctorsComboBox.SelectedValue.ToString();

                //setting appoitnment values
                appointment.PATIENT_ID = Convert.ToInt32(SELECTED_PATIENT);
                appointment.DOCTOR_ID = Convert.ToInt32(SELECTED_DOCTOR);

                //create appoitment
                appointment.Create();
            }
        }

        private void btnPrintInvoice_Click(object sender, EventArgs e)
        {
            //selected patient id 
            string SELECTED_PATIENT = gridCompletedAppointments.SelectedRows[0].Cells[0].Value.ToString();

            MessageBox.Show(SELECTED_PATIENT);
        }
    }
}
