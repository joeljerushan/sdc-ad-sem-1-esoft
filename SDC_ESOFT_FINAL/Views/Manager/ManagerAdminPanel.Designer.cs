﻿namespace SDC_ESOFT_FINAL.Views.Manager
{
    partial class ManagerAdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.userExpYears = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.userNic = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.userDateOfBirth = new MetroFramework.Controls.MetroDateTime();
            this.userPasswordRep = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.userPassword = new MetroFramework.Controls.MetroTextBox();
            this.userAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.userEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.userPhone = new MetroFramework.Controls.MetroTextBox();
            this.userName = new MetroFramework.Controls.MetroTextBox();
            this.createUserButton = new MetroFramework.Controls.MetroButton();
            this.userDeleteButton = new MetroFramework.Controls.MetroButton();
            this.dataGridViewDoctors = new System.Windows.Forms.DataGridView();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDoctors)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Location = new System.Drawing.Point(12, 12);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1098, 568);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel10);
            this.metroTabPage1.Controls.Add(this.userExpYears);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.userNic);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.userDateOfBirth);
            this.metroTabPage1.Controls.Add(this.userPasswordRep);
            this.metroTabPage1.Controls.Add(this.metroLabel9);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.userPassword);
            this.metroTabPage1.Controls.Add(this.userAddress);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel7);
            this.metroTabPage1.Controls.Add(this.userEmail);
            this.metroTabPage1.Controls.Add(this.metroLabel8);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.userPhone);
            this.metroTabPage1.Controls.Add(this.userName);
            this.metroTabPage1.Controls.Add(this.createUserButton);
            this.metroTabPage1.Controls.Add(this.userDeleteButton);
            this.metroTabPage1.Controls.Add(this.dataGridViewDoctors);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1090, 526);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Doctors";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(655, 429);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(101, 19);
            this.metroLabel10.TabIndex = 59;
            this.metroLabel10.Text = "Experience Year";
            // 
            // userExpYears
            // 
            this.userExpYears.DisplayFocus = true;
            this.userExpYears.FormattingEnabled = true;
            this.userExpYears.ItemHeight = 23;
            this.userExpYears.Items.AddRange(new object[] {
            "1 Year",
            "2 Years",
            "5 Years and Up",
            "10 Years and Up",
            "20 Years and Up",
            "25 Years and Up",
            "None"});
            this.userExpYears.Location = new System.Drawing.Point(654, 451);
            this.userExpYears.Name = "userExpYears";
            this.userExpYears.Size = new System.Drawing.Size(192, 29);
            this.userExpYears.TabIndex = 58;
            this.userExpYears.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(872, 367);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(84, 19);
            this.metroLabel1.TabIndex = 51;
            this.metroLabel1.Text = "NIC Number";
            // 
            // userNic
            // 
            // 
            // 
            // 
            this.userNic.CustomButton.Image = null;
            this.userNic.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userNic.CustomButton.Name = "";
            this.userNic.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userNic.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userNic.CustomButton.TabIndex = 1;
            this.userNic.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userNic.CustomButton.UseSelectable = true;
            this.userNic.CustomButton.Visible = false;
            this.userNic.Lines = new string[0];
            this.userNic.Location = new System.Drawing.Point(873, 389);
            this.userNic.MaxLength = 32767;
            this.userNic.Name = "userNic";
            this.userNic.PasswordChar = '\0';
            this.userNic.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userNic.SelectedText = "";
            this.userNic.SelectionLength = 0;
            this.userNic.SelectionStart = 0;
            this.userNic.ShortcutsEnabled = true;
            this.userNic.Size = new System.Drawing.Size(191, 23);
            this.userNic.TabIndex = 50;
            this.userNic.UseSelectable = true;
            this.userNic.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userNic.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(654, 367);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(115, 19);
            this.metroLabel5.TabIndex = 46;
            this.metroLabel5.Text = "Confirm Password";
            // 
            // userDateOfBirth
            // 
            this.userDateOfBirth.Location = new System.Drawing.Point(438, 451);
            this.userDateOfBirth.MinimumSize = new System.Drawing.Size(0, 29);
            this.userDateOfBirth.Name = "userDateOfBirth";
            this.userDateOfBirth.Size = new System.Drawing.Size(190, 29);
            this.userDateOfBirth.TabIndex = 57;
            // 
            // userPasswordRep
            // 
            // 
            // 
            // 
            this.userPasswordRep.CustomButton.Image = null;
            this.userPasswordRep.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPasswordRep.CustomButton.Name = "";
            this.userPasswordRep.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPasswordRep.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPasswordRep.CustomButton.TabIndex = 1;
            this.userPasswordRep.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPasswordRep.CustomButton.UseSelectable = true;
            this.userPasswordRep.CustomButton.Visible = false;
            this.userPasswordRep.Lines = new string[0];
            this.userPasswordRep.Location = new System.Drawing.Point(655, 389);
            this.userPasswordRep.MaxLength = 32767;
            this.userPasswordRep.Name = "userPasswordRep";
            this.userPasswordRep.PasswordChar = '\0';
            this.userPasswordRep.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPasswordRep.SelectedText = "";
            this.userPasswordRep.SelectionLength = 0;
            this.userPasswordRep.SelectionStart = 0;
            this.userPasswordRep.ShortcutsEnabled = true;
            this.userPasswordRep.Size = new System.Drawing.Size(191, 23);
            this.userPasswordRep.TabIndex = 45;
            this.userPasswordRep.UseSelectable = true;
            this.userPasswordRep.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPasswordRep.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(437, 429);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(84, 19);
            this.metroLabel9.TabIndex = 56;
            this.metroLabel9.Text = "Date of birth";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(435, 367);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(63, 19);
            this.metroLabel4.TabIndex = 44;
            this.metroLabel4.Text = "Password";
            // 
            // userPassword
            // 
            // 
            // 
            // 
            this.userPassword.CustomButton.Image = null;
            this.userPassword.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPassword.CustomButton.Name = "";
            this.userPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPassword.CustomButton.TabIndex = 1;
            this.userPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPassword.CustomButton.UseSelectable = true;
            this.userPassword.CustomButton.Visible = false;
            this.userPassword.Lines = new string[0];
            this.userPassword.Location = new System.Drawing.Point(436, 389);
            this.userPassword.MaxLength = 32767;
            this.userPassword.Name = "userPassword";
            this.userPassword.PasswordChar = '\0';
            this.userPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPassword.SelectedText = "";
            this.userPassword.SelectionLength = 0;
            this.userPassword.SelectionStart = 0;
            this.userPassword.ShortcutsEnabled = true;
            this.userPassword.Size = new System.Drawing.Size(191, 23);
            this.userPassword.TabIndex = 43;
            this.userPassword.UseSelectable = true;
            this.userPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // userAddress
            // 
            // 
            // 
            // 
            this.userAddress.CustomButton.Image = null;
            this.userAddress.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userAddress.CustomButton.Name = "";
            this.userAddress.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userAddress.CustomButton.TabIndex = 1;
            this.userAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userAddress.CustomButton.UseSelectable = true;
            this.userAddress.CustomButton.Visible = false;
            this.userAddress.Lines = new string[0];
            this.userAddress.Location = new System.Drawing.Point(6, 451);
            this.userAddress.MaxLength = 32767;
            this.userAddress.Name = "userAddress";
            this.userAddress.PasswordChar = '\0';
            this.userAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userAddress.SelectedText = "";
            this.userAddress.SelectionLength = 0;
            this.userAddress.SelectionStart = 0;
            this.userAddress.ShortcutsEnabled = true;
            this.userAddress.Size = new System.Drawing.Size(191, 23);
            this.userAddress.TabIndex = 52;
            this.userAddress.UseSelectable = true;
            this.userAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(223, 367);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 42;
            this.metroLabel3.Text = "Email";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(5, 429);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(56, 19);
            this.metroLabel7.TabIndex = 53;
            this.metroLabel7.Text = "Address";
            // 
            // userEmail
            // 
            // 
            // 
            // 
            this.userEmail.CustomButton.Image = null;
            this.userEmail.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userEmail.CustomButton.Name = "";
            this.userEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userEmail.CustomButton.TabIndex = 1;
            this.userEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userEmail.CustomButton.UseSelectable = true;
            this.userEmail.CustomButton.Visible = false;
            this.userEmail.Lines = new string[0];
            this.userEmail.Location = new System.Drawing.Point(224, 389);
            this.userEmail.MaxLength = 32767;
            this.userEmail.Name = "userEmail";
            this.userEmail.PasswordChar = '\0';
            this.userEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userEmail.SelectedText = "";
            this.userEmail.SelectionLength = 0;
            this.userEmail.SelectionStart = 0;
            this.userEmail.ShortcutsEnabled = true;
            this.userEmail.Size = new System.Drawing.Size(191, 23);
            this.userEmail.TabIndex = 41;
            this.userEmail.UseSelectable = true;
            this.userEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(223, 429);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(99, 19);
            this.metroLabel8.TabIndex = 55;
            this.metroLabel8.Text = "Phone Number";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(4, 367);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(75, 19);
            this.metroLabel2.TabIndex = 40;
            this.metroLabel2.Text = "User Name";
            // 
            // userPhone
            // 
            // 
            // 
            // 
            this.userPhone.CustomButton.Image = null;
            this.userPhone.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userPhone.CustomButton.Name = "";
            this.userPhone.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userPhone.CustomButton.TabIndex = 1;
            this.userPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userPhone.CustomButton.UseSelectable = true;
            this.userPhone.CustomButton.Visible = false;
            this.userPhone.Lines = new string[0];
            this.userPhone.Location = new System.Drawing.Point(224, 451);
            this.userPhone.MaxLength = 32767;
            this.userPhone.Name = "userPhone";
            this.userPhone.PasswordChar = '\0';
            this.userPhone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userPhone.SelectedText = "";
            this.userPhone.SelectionLength = 0;
            this.userPhone.SelectionStart = 0;
            this.userPhone.ShortcutsEnabled = true;
            this.userPhone.Size = new System.Drawing.Size(191, 23);
            this.userPhone.TabIndex = 54;
            this.userPhone.UseSelectable = true;
            this.userPhone.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userPhone.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // userName
            // 
            // 
            // 
            // 
            this.userName.CustomButton.Image = null;
            this.userName.CustomButton.Location = new System.Drawing.Point(169, 1);
            this.userName.CustomButton.Name = "";
            this.userName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.userName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.userName.CustomButton.TabIndex = 1;
            this.userName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.userName.CustomButton.UseSelectable = true;
            this.userName.CustomButton.Visible = false;
            this.userName.Lines = new string[0];
            this.userName.Location = new System.Drawing.Point(5, 389);
            this.userName.MaxLength = 32767;
            this.userName.Name = "userName";
            this.userName.PasswordChar = '\0';
            this.userName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.userName.SelectedText = "";
            this.userName.SelectionLength = 0;
            this.userName.SelectionStart = 0;
            this.userName.ShortcutsEnabled = true;
            this.userName.Size = new System.Drawing.Size(191, 23);
            this.userName.TabIndex = 39;
            this.userName.UseSelectable = true;
            this.userName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.userName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // createUserButton
            // 
            this.createUserButton.Location = new System.Drawing.Point(6, 497);
            this.createUserButton.Name = "createUserButton";
            this.createUserButton.Size = new System.Drawing.Size(193, 23);
            this.createUserButton.TabIndex = 36;
            this.createUserButton.Text = "Create New Doctor";
            this.createUserButton.UseSelectable = true;
            this.createUserButton.Click += new System.EventHandler(this.createUserButton_Click);
            // 
            // userDeleteButton
            // 
            this.userDeleteButton.Location = new System.Drawing.Point(223, 500);
            this.userDeleteButton.Name = "userDeleteButton";
            this.userDeleteButton.Size = new System.Drawing.Size(194, 23);
            this.userDeleteButton.TabIndex = 38;
            this.userDeleteButton.Text = "Delete Doctor";
            this.userDeleteButton.UseSelectable = true;
            this.userDeleteButton.Click += new System.EventHandler(this.userDeleteButton_Click);
            // 
            // dataGridViewDoctors
            // 
            this.dataGridViewDoctors.AllowUserToAddRows = false;
            this.dataGridViewDoctors.AllowUserToDeleteRows = false;
            this.dataGridViewDoctors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDoctors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDoctors.Location = new System.Drawing.Point(0, 3);
            this.dataGridViewDoctors.Name = "dataGridViewDoctors";
            this.dataGridViewDoctors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDoctors.Size = new System.Drawing.Size(1087, 346);
            this.dataGridViewDoctors.TabIndex = 2;
            this.dataGridViewDoctors.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDoctors_CellContentClick);
            // 
            // ManagerAdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1122, 603);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "ManagerAdminPanel";
            this.Text = "ManagerAdminPanel";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ManagerAdminPanel_FormClosed);
            this.Load += new System.EventHandler(this.ManagerAdminPanel_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDoctors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private System.Windows.Forms.DataGridView dataGridViewDoctors;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox userExpYears;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox userNic;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroDateTime userDateOfBirth;
        private MetroFramework.Controls.MetroTextBox userPasswordRep;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox userPassword;
        private MetroFramework.Controls.MetroTextBox userAddress;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox userEmail;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox userPhone;
        private MetroFramework.Controls.MetroTextBox userName;
        private MetroFramework.Controls.MetroButton createUserButton;
        private MetroFramework.Controls.MetroButton userDeleteButton;
    }
}