﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SDC_ESOFT_FINAL.Classes;

namespace SDC_ESOFT_FINAL.Views.Manager
{
    public partial class ManagerAdminPanel : Form
    {
        User user = new User();

        public ManagerAdminPanel()
        {
            InitializeComponent();

            //on load load, data table by passing doctor role id 3
            user.SortDoctors(dataGridViewDoctors, 3);
        }

        private void btnClose_Click(object sender, EventArgs eventArgs)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void createUserButton_Click(object sender, EventArgs e)
        {

            string name = userName.Text;
            string email = userEmail.Text;
            string password = userPassword.Text;
            string password_repeat = userPasswordRep.Text;
            string nic = userNic.Text;
            string address = userAddress.Text;
            string phone = userPhone.Text;
            string date_of_birth = userDateOfBirth.Text;
            int exp_years = userExpYears.SelectedIndex + 1;

            if (string.IsNullOrEmpty(name))
            {
                MessageBox.Show("Name Required");
            }
            else if (string.IsNullOrEmpty(email))
            {
                MessageBox.Show("Email Required");
            }
            else if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password Required");
            }
            else
            {
                if (password == password_repeat)
                {
                    user.USER_NAME = name;
                    user.USER_EMAIL = email;
                    user.USER_PASSWORD = password;
                    user.USER_NIC = nic;
                    user.USER_ADDRESS = address;
                    user.USER_PHONE = phone;
                    user.USER_DATE_OF_BIRTH = date_of_birth;
                    user.USER_EXP_YEARS = exp_years;
                    user.ACCESS_ROLE = 3;

                    //creating user function
                    user.CreateUser();

                    //clear current values in text box
                    userName.Text = "";
                    userEmail.Text = "";
                    userPassword.Text = "";
                    userPasswordRep.Text = "";
                    userExpYears.SelectedIndex = 6;
                    userNic.Text = "";
                    userAddress.Text = "";
                    userPhone.Text = "";
                    userDateOfBirth.Text = "";

                    //reload data table by passing doctor role id 3
                    user.SortDoctors(dataGridViewDoctors, 3);

                    //show success message
                    MessageBox.Show("Doctor Created");
                }
                else
                {
                    MessageBox.Show("Password Don't Match");
                }
            }
        }

        private void userDeleteButton_Click(object sender, EventArgs e)
        {
            // get currently focus row id and set it to a string 
            string DELETE_ID = dataGridViewDoctors.SelectedRows[0].Cells[0].Value.ToString();

            //set user class user id 
            user.USER_ID = DELETE_ID;

            // call delete user function
            user.DeleteUser();

            //reload data table
            user.SortDoctors(dataGridViewDoctors, 3);

            //show success message 
            MessageBox.Show("Doctor Removed");
        }

        private void ManagerAdminPanel_Load(object sender, EventArgs e)
        {

        }

        private void ManagerAdminPanel_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.ExitThread();
        }

        private void dataGridViewDoctors_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
